# Safety PM Rating Guideline  

|Scenario|Actual number of crashes vs. baseline 5-year rolling average|5-year rolling average vs. baseline 5-year rolling average|5-year rolling average vs. annual objective|Rating                  |
|--------|------------------------------------------------------------|----------------------------------------------------------|-------------------------------------------|--------                |
|1       |Equal or lower                                              |Equal or lower                                            |Equal or lower                             |Positive                |
|2       |Equal or lower                                              |Equal or lower                                            |Higher                                     |Neutral                 |
|3       |Equal or lower                                              |Higher                                                    |Equal or lower                             |Not a feasible scenario |    |
|4       |Equal or lower                                              |Higher                                                    |Higher                                     |Neutral                 |
|5       |Higher                                                      |Equal or lower                                            |Equal or lower                             |Positive                |
|6       |Higher                                                      |Equal or lower                                            |Higher                                     |Negative                |
|7       |Higher                                                      |Higher                                                    |Equal or lower                             |Not a feasible scenario |
|8       |Higher                                                      |Higher                                                    |Higher                                     |Negative                |
